echo "${green}***************************************************************************************"
echo '***************************************************************************************'
echo '*  This uses sed command to replace content and this works well with gnu-sed version  *'
echo '* So run this command before running this : brew install gnu-sed --with-default-names *'
echo '***************************************************************************************'
echo "***************************************************************************************${reset}"
echo ""
echo ""

# Colors
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

echo "Creating temp folder if it doesn't exists..."
mkdir -p temp

echo "Entering in temp folder..."
cd temp

echo ""
echo -n "${green}Enter desired project name: ${reset}"
read PROJECT

echo ""
echo "${green}Creating django project with current version with name : ${reset}"$PROJECT

echo -n "${red}Current installed version of Django is : ${reset}"
python -m django --version

echo ""
echo -n "${green}Creating Django Project with command : django-admin startproject $PROJECT ${reset}"
django-admin startproject $PROJECT

echo ""

echo "${red} ${reset}"
echo "${red}Entering in project directory >>> ${reset}"
cd $PROJECT

echo ""

echo -n "${green}Enter desired app name: ${reset}"
read APP
python manage.py startapp $APP


echo "Editing settings.py"
# sed '/^anothervalue=.*/a after=me' test.txt

echo "/django.contrib.staticfiles/a,'$PROJECT',"

sed "s/'django.contrib.staticfiles',/'django.contrib.staticfiles','$PROJECT',/" $PROJECT/settings.py

echo ""
echo "${green}Do you want to collect the static files in your application : ${reset}"
python manage.py collectstatic

echo "${green}Project creation completed... ${reset}"

echo ""
echo ""

echo "${green}Coming back to original directory ${reset}"
cd ../..

echo "${green}Project creation completed... ${reset}"
echo "${red} ${reset}"